## Description du contenu de ce dépôt

Ce dépôt contient
- le code de l'application ICD météo
- le code nécessaire à la construction de l'image de l'application ICD météo
- un fichier docker-compose qui permet de lancer l'application ICD météo
- le code de la chaine d'intégration continue et de livraison continue qui permet de tester l'application ICD meteo et de livrer son image

## Application ICD météo

ICD météo est une application qui fournit des prévisions météo "sonifiées". Elle est responsive et accessible par les déficients visuels.

L'application ICD météo est codée en Javascript via l'utilisation du framework Vuetify/VueJS.

L'architecture de l'application se limite à un frontend.



## Chaine d'intégration continue et de livraison continue

La chaine réalise des tests statiques, des tests dynamiques et publie le résulat des tests dans l'espace https://projet-industriel.gitlab-pages.imt-atlantique.fr/app-meteo/

Tests statiques:
 - test de la qualité du code VueJS
 - test du code du Dockerfile
 - test du code de la chaine chaine d'intégration continue et de livraison continue

Tests dynamiques:
 - Test e2e qui vérifie que le nom de ville dont on souhaite connaitre la prévision météo apparait dans l'écart qui fournit les prévisions météo 

L'image construite par la chaine est livrée dans le registre de conteneur du dépôt : https://gitlab.imt-atlantique.fr/projet-industriel/app-meteo/container_registry


## Utilisation de ce dépôt

Lancement de la chaine pour tester la qualité du code poussé sur le dépôt 
 - Allez dans le menu "CI/CD"/"Pipelines"
 - Une fois la page "Pipelines"ouverte, cliquer en haut à droite sur le bouton bleu "Run Pipeline"
 - Sur la page qui s'ouvre, cliquez à nouveau sur le bouton bleu "Run Pipeline"
 

Livraison de l'image de l'application ICD météo
  - La livraison de l'image de l'application ICD météo se déclenche manuellement
  - Aller dans le pipeline que vous avez lancé et cliquez sur la fléche associée au job "deliver"
