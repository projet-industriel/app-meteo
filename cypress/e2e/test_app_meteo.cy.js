describe('Test app meteo', () => {
  it('visits the app root url', () => {
    cy.visit('/')
    cy.contains('h2', 'Ville')
    cy.get(".ma-3").contains('Géolocalisation').click()
    cy.get(".ma-0").first().click()
    cy.get(".ma-3").contains('Valider').click()
    cy.get(".text-h3").should("contain", "Plouzané")
  })
})