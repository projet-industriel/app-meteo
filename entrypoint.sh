#! /bin/sh
set -e
envsubst '${SUBNET_STUB_STATUS}' < /etc/nginx/conf.d/nginx-stub_status.conf.template > /etc/nginx/conf.d/nginx-stub_status.conf
exec "$@"