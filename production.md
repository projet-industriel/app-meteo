# Déploiement en mode production

## Description

Pour le déploiement en mode production, le frontend est géré par un serveur web dédié.  

Détails:

* Frontend :
  * utilise le serveur WEB Nginx
  * application statique
  * envoie des requetes vers les APIs externes depuis le navigateur de l'utilisateur

## Préparation du système

* mise à jour du système

``` bash
sudo apt update
```

* Création de l'utilisateur "app-meteo"

``` bash
sudo useradd \
   -m -d /home/app-meteo \
   -r -c 'Meteo App system-user' \
   -s /bin/bash \
   app-meteo
```

* Installation des pré-requis

``` bash
sudo apt -y install git vim nano
```

## Initialisation de l'environnement

* Connexion en tant qu'utilisateur app-meteo

``` bash
sudo -i -u app-meteo
```

* Vérification

``` bash
id 
# uid=998(app-meteo) gid=998(app-meteo) groups=998(app-meteo)
```

* Installation de NVM (Node Version Manager)

```sh
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
```

* Vérification

```sh
command -v nvm
```

* Installation de Node JS

```sh
nvm install --lts
```

## Installation de l'application

* Récupération du dépot

```bash
cd $HOME
git clone https://gitlab.imt-atlantique.fr/rtb10361/projet_site_web_meteo_msicd.git icd-meteo
```

* Installation des dépendances Node JS  

```bash
cd $HOME/icd-meteo
npm install
```

* Compilation de l'application  

```sh
npm run build
```

Le répertoire __dist/__ est généré avec les fichiers statiques de l'application web  

## Déploiement de l'application

### Installation du serveur WEB Nginx

Nécéssite des droits "sudo", que l'utilisateur 'app-meteo' n'a pas.

```bash
exit
```

``` bash
sudo apt -y install nginx-light
```

* Test de Nginx

Depuis un navigateur : <http://localhost:80>  

> Rem : si vous ne travaillez pas sur votre machine locale, utilisez l'adresse IP publique de votre VM, ou de votre instance Cloud.  

### Génération du fichier de configuration du serveur Nginx

* Connexion an tant qu'utilisateur app-meteo  

```bash
sudo -i -u app-meteo

cd $HOME/icd-meteo
pwd
```

``` bash
export METEO_URL_SERVERNAME=0.0.0.0
export METEO_URL_PORT=8000
export METEO_FRONTEND_ROOT=${PWD}/dist

envsubst '${METEO_URL_SERVERNAME},${METEO_URL_PORT},${METEO_FRONTEND_ROOT}' < nginx.conf.template > nginx.conf

cp nginx.conf /tmp
```

### Mise en place des pages

Nécéssite des droits "sudo", que l'utilisateur 'app-meteo' n'a pas.

```bash
exit
```

* Ajout de l'utilisateur www-data au groupe app-meteo  

```bash
sudo usermod -a -G app-meteo www-data
```

* Mise en place du fichier de configuration  

```bash
sudo cp /tmp/nginx.conf /etc/nginx/sites-available/meteo
sudo ln -s /etc/nginx/sites-available/meteo /etc/nginx/sites-enabled/
sudo systemctl restart nginx
```

## Test de l'accès à l'application

Depuis un navigateur : <http://localhost:8000>  

> Rem : si vous ne travaillez pas sur votre machine locale, utilisez l'adresse IP publique de votre VM, ou de votre instance Cloud.
